package ru.karamyshev.taskmanager.repository;

import org.jetbrains.annotations.NotNull;
import ru.karamyshev.taskmanager.entity.AbstractEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    public void clear() {
        entityManager.clear();
    }

    public void add(final @NotNull List<E> es) {
        for (final E e: es){
            if (e == null) return;
            entityManager.persist(e);
        }
    }

    public void add(@NotNull final E... es) {
        for (@NotNull final E e: es) entityManager.persist(e);
    }

    public void merge(@NotNull final E... es) {
        for (@NotNull final E e: es) entityManager.merge(e);
    }

    public void load(@NotNull final List<E> es) {
        clear();
        add(es);
    }

}
