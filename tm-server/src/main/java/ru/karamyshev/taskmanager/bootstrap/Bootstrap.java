package ru.karamyshev.taskmanager.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.karamyshev.taskmanager.api.endpoint.*;
import ru.karamyshev.taskmanager.api.service.IPropertyService;
import ru.karamyshev.taskmanager.api.service.IUserService;

import javax.xml.ws.Endpoint;

@Component
@NoArgsConstructor
public class Bootstrap {

    private IPropertyService propertyService;

    private IUserService userService;

    private ITaskEndpoint taskEndpoint;

    private IUserEndpoint userEndpoint;

    private IAdminEndpoint adminEndpoint;

    private IAdminUserEndpoint adminUserEndpoint;

    private IProjectEndpoint projectEndpoint;

    private ISessionEndpoint sessionEndpoint;

    @Autowired
    public Bootstrap(
            @NotNull final IPropertyService propertyService,
            @NotNull final IUserService userService,
            @NotNull final ITaskEndpoint taskEndpoint,
            @NotNull final IUserEndpoint userEndpoint,
            @NotNull final IAdminEndpoint adminEndpoint,
            @NotNull final IAdminUserEndpoint adminUserEndpoint,
            @NotNull final IProjectEndpoint projectEndpoint,
            @NotNull final ISessionEndpoint sessionEndpoint
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
        this.taskEndpoint = taskEndpoint;
        this.userEndpoint = userEndpoint;
        this.adminEndpoint = adminEndpoint;
        this.adminUserEndpoint = adminUserEndpoint;
        this.projectEndpoint = projectEndpoint;
        this.sessionEndpoint = sessionEndpoint;
    }

    private void initEndpoint() {
        @NotNull final String url = "http://" + propertyService.getServerHost() + ":" + propertyService.getServerPort();
        @NotNull final String sessionEndpointUrl = url + "/SessionEndpoint?wsdl";
        @NotNull final String taskEndpointUrl = url + "/TaskEndpoint?wsdl";
        @NotNull final String projectEndpointUrl = url + "/ProjectEndpoint?wsdl";
        @NotNull final String userEndpointUrl = url + "/UserEndpoint?wsdl";
        @NotNull final String adminUserEndpointUrl = url + "/AdminUserEndpoint?wsdl";
        @NotNull final String adminEndpointUrl = url + "/AdminEndpoint?wsdl";

        Endpoint.publish(sessionEndpointUrl, sessionEndpoint);
        Endpoint.publish(taskEndpointUrl, taskEndpoint);
        Endpoint.publish(projectEndpointUrl, projectEndpoint);
        Endpoint.publish(userEndpointUrl, userEndpoint);
        Endpoint.publish(adminEndpointUrl, adminEndpoint);
        Endpoint.publish(adminUserEndpointUrl, adminUserEndpoint);

        System.out.println(sessionEndpointUrl);
        System.out.println(taskEndpointUrl);
        System.out.println(projectEndpointUrl);
        System.out.println(userEndpointUrl);
        System.out.println(adminUserEndpointUrl);
        System.out.println(adminEndpointUrl);
    }


    public void run(final String[] args) {
        initEndpoint();
        System.out.println("*** TASK MANAGER SERVER ***");
    }

}
