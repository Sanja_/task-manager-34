package ru.karamyshev.taskmanager.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.karamyshev.taskmanager.api.service.IDomainService;
import ru.karamyshev.taskmanager.api.service.IProjectService;
import ru.karamyshev.taskmanager.api.service.ITaskService;
import ru.karamyshev.taskmanager.api.service.IUserService;
import ru.karamyshev.taskmanager.dto.Domain;

@Service
public class DomainService implements IDomainService {

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private ITaskService taskService;

    @Nullable
    @Autowired
    private IProjectService projectService;

    public DomainService(
            @NotNull final IUserService userService,
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService
    ) {
        this.userService = userService;
        this.taskService = taskService;
        this.projectService = projectService;
    }

    @Override
    @Transactional
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        // projectService.load(domain.getProjects());
        // taskService.load(domain.getTasks());
        // userService.load(domain.getUsers());
    }

    @Override
    public void export(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
    }

}
